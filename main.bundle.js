webpackJsonp([1,5],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__(35);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContextComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContextComponent = (function () {
    function ContextComponent(router, route, config) {
        this.router = router;
        this.route = route;
        this.config = config;
    }
    ContextComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.queryParams.subscribe(function (params) {
            _this.serviceId = params['service'];
            _this.usecaseId = +params['usecase'];
        });
        this.config.getData().subscribe(function (res) {
            _this.services = res.json();
        });
        this.background = {
            'background-size': 'cover',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
            'background-image': 'url(assets/background/context.png)',
        };
    };
    return ContextComponent;
}());
ContextComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-context',
        template: __webpack_require__(177),
        styles: [__webpack_require__(166)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* Config */]) === "function" && _c || Object])
], ContextComponent);

var _a, _b, _c;
//# sourceMappingURL=context.component.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EndComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EndComponent = (function () {
    function EndComponent() {
    }
    EndComponent.prototype.ngOnInit = function () {
        this.background = {
            'background-size': 'cover',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
            'background-image': 'url(assets/background/common.png)',
        };
    };
    return EndComponent;
}());
EndComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-end',
        template: __webpack_require__(178),
        styles: [__webpack_require__(167)]
    }),
    __metadata("design:paramtypes", [])
], EndComponent);

//# sourceMappingURL=end.component.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__(35);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupComponent = (function () {
    function PopupComponent(router, route, config) {
        this.router = router;
        this.route = route;
        this.config = config;
        this.slider = true;
        this.back = false;
        this.usecase = false;
        this.paragraph = true;
        this.pagination = true;
    }
    PopupComponent.prototype.comeBack = function () {
        this.slider = true;
        this.back = false;
        this.usecase = false;
        this.paragraph = true;
        this.pagination = true;
    };
    PopupComponent.prototype.SetUseCase = function () {
        this.slider = false;
        this.back = true;
        this.usecase = true;
        this.paragraph = false;
        this.pagination = false;
    };
    PopupComponent.prototype.nextContext = function (service, usecase) {
        this.router.navigate(['/context'], { queryParams: { service: service, usecase: usecase } });
    };
    PopupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.queryParams.subscribe(function (params) {
            _this.id = params['service'];
        });
        this.config.getData().subscribe(function (res) {
            _this.services = res.json();
        });
    };
    PopupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return PopupComponent;
}());
PopupComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-popup',
        template: __webpack_require__(179),
        styles: [__webpack_require__(168)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* Config */]) === "function" && _c || Object])
], PopupComponent);

var _a, _b, _c;
//# sourceMappingURL=popup.component.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreambleComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PreambleComponent = (function () {
    function PreambleComponent() {
    }
    PreambleComponent.prototype.ngOnInit = function () {
        this.background = {
            'background-size': 'cover',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
            'background-image': 'url(assets/background/common.png)',
        };
    };
    return PreambleComponent;
}());
PreambleComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-preamble',
        template: __webpack_require__(180),
        styles: [__webpack_require__(169)]
    }),
    __metadata("design:paramtypes", [])
], PreambleComponent);

//# sourceMappingURL=preamble.component.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceComponent = (function () {
    function ServiceComponent(router, route, domSanitizer) {
        this.router = router;
        this.route = route;
        this.domSanitizer = domSanitizer;
        this.hidden = true;
    }
    ServiceComponent.prototype.menu = function () {
        this.hidden ? this.hidden = false : this.hidden = true;
    };
    ServiceComponent.prototype.goBack = function () {
        window.history.go(-2);
    };
    ServiceComponent.prototype.setVideo = function () {
        this.enableVideo = true;
        this.enableIframe = false;
        this.url = this.param;
        this.background = {
            'background-size': 'cover',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
            'background-image': 'url(assets/background/common.png)',
        };
    };
    ServiceComponent.prototype.setIframe = function () {
        if (this.param == 'ged-numerisation') {
            this.windowOpened = window.open("http://awt.bddf.applis.bad.socgen/show-room-statics/iged/IGED_index.html", "_new");
            console.log(this.windowOpened);
            setTimeout(function (windowOpened) {
                console.log(windowOpened);
                windowOpened.document.body.innerHTML += "<button id='niji_close_button'>fermer la fenêtre</button>";
                var btn = windowOpened.document.getElementById("niji_close_button");
                btn.onclick = function () {
                    windowOpened.close();
                };
            }, 2000);
        }
        else {
            this.enableVideo = false;
            this.enableIframe = true;
            this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.param);
        }
    };
    ServiceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.queryParams.subscribe(function (params) {
            _this.param = params['url'];
            /mp4/.test(_this.param) ? _this.setVideo() : _this.setIframe();
        });
    };
    ServiceComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return ServiceComponent;
}());
ServiceComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-service',
        template: __webpack_require__(181),
        styles: [__webpack_require__(170)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */]) === "function" && _c || Object])
], ServiceComponent);

var _a, _b, _c;
//# sourceMappingURL=service.component.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SliderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SliderComponent = (function () {
    function SliderComponent(router, route) {
        this.router = router;
        this.route = route;
        this.config = {
            initialSlide: 5,
            slidesPerView: 10,
            effect: 'coverflow',
            centeredSlides: true,
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            slideToClickedSlide: true,
            keyboardControl: true,
            coverflow: {
                rotate: 0,
                stretch: -30,
                depth: 300,
                modifier: 1,
                slideShadows: false
            }
        };
        this.config2 = {
            initialSlide: 1,
            slidesPerView: 1,
            centeredSlides: true,
            keyboardControl: true,
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next'
        };
        this.harmonie = 'harmonie';
        this.ged = 'ged';
        this.esignature = 'esignature';
        this.radlad = 'radlad';
        this.sdcpj = 'sdcpj';
        this.rpa = 'rpa';
        this.alerting = 'alerting';
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = true;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
        // this.config['initialSlide'] = 3;
    }
    SliderComponent.prototype.nextPopup = function (event, service) {
        var eventClass = event.currentTarget.classList;
        var self = this;
        var delay = 700; //ms
        setTimeout(function () {
            if (eventClass.contains('swiper-slide-active')) {
                self.router.navigate(['popup'], { relativeTo: self.route, queryParams: { service: service } });
            }
        }, delay);
    };
    SliderComponent.prototype.isActive = function (event) {
        var eventClass = event.currentTarget.classList;
        console.log(eventClass);
        if (eventClass.contains('swiper-slide-active')) {
            return true;
        }
        else {
            return false;
        }
    };
    SliderComponent.prototype.activateEsignature = function () {
        this.esignatureIsActive = true;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = false;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateSdcpj = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = true;
        this.radladIsActive = false;
        this.rpaIsActive = false;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateRadlad = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = true;
        this.rpaIsActive = false;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateRpa = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = true;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateGed = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = false;
        this.gedIsActive = true;
        this.harmonieIsActive = false;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateHarmonie = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = false;
        this.gedIsActive = false;
        this.harmonieIsActive = true;
        this.alertingIsActive = false;
    };
    SliderComponent.prototype.activateAlerting = function () {
        this.esignatureIsActive = false;
        this.sdcpjIsActive = false;
        this.radladIsActive = false;
        this.rpaIsActive = false;
        this.gedIsActive = false;
        this.harmonieIsActive = false;
        this.alertingIsActive = true;
    };
    SliderComponent.prototype.ngOnInit = function () { };
    return SliderComponent;
}());
SliderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-slider',
        template: __webpack_require__(182),
        styles: [__webpack_require__(171)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object])
], SliderComponent);

var _a, _b;
//# sourceMappingURL=slider.component.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StartComponent = (function () {
    function StartComponent() {
    }
    StartComponent.prototype.ngOnInit = function () {
        this.background = {
            'background-size': 'cover',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
            'background-image': 'url(assets/background/common.png)',
        };
    };
    return StartComponent;
}());
StartComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-start',
        template: __webpack_require__(183),
        styles: [__webpack_require__(172)]
    }),
    __metadata("design:paramtypes", [])
], StartComponent);

//# sourceMappingURL=start.component.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, ".button.red {\n  position: fixed;\n  right: 30px;\n  bottom: 30px;\n  z-index: 10;\n  -webkit-transform: scale(0);\n          transform: scale(0);\n  -webkit-animation: show 1s ease 0.5s forwards;\n          animation: show 1s ease 0.5s forwards; }\n\n@-webkit-keyframes show {\n  0% {\n    -webkit-transform: scale(0);\n            transform: scale(0); }\n  100% {\n    -webkit-transform: scale(1);\n            transform: scale(1); } }\n\n@keyframes show {\n  0% {\n    -webkit-transform: scale(0);\n            transform: scale(0); }\n  100% {\n    -webkit-transform: scale(1);\n            transform: scale(1); } }\n\n.img-architecture {\n  max-width: 80%;\n  margin: 0 auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "a {\n  cursor: pointer; }\n\n.persona {\n  position: absolute;\n  top: -180px;\n  left: 1100px;\n  z-index: 10;\n  width: 350px;\n  height: 363px;\n  border-radius: 50%;\n  background-color: #FFFFFF;\n  overflow: hidden; }\n\n.img-persona {\n  position: absolute;\n  left: 0;\n  bottom: -100px;\n  z-index: 20;\n  width: 350px;\n  -webkit-animation: persona 2.5s linear 0s infinite alternate;\n          animation: persona 2.5s linear 0s infinite alternate; }\n\n@-webkit-keyframes persona {\n  0% {\n    bottom: -50px; }\n  100% {\n    bottom: -30px; } }\n\n@keyframes persona {\n  0% {\n    bottom: -50px; }\n  100% {\n    bottom: -30px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "@-webkit-keyframes moveStar {\n  0% {\n    -webkit-transform: scale3d(0, 0, 0);\n            transform: scale3d(0, 0, 0); }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n            transform: scale3d(1, 1, 1); } }\n\n@keyframes moveStar {\n  0% {\n    -webkit-transform: scale3d(0, 0, 0);\n            transform: scale3d(0, 0, 0); }\n  100% {\n    -webkit-transform: scale3d(1, 1, 1);\n            transform: scale3d(1, 1, 1); } }\n\n.animation {\n  position: fixed;\n  right: 100px;\n  bottom: 100px;\n  z-index: 10; }\n\n.star-animation {\n  position: absolute;\n  z-index: 10;\n  -webkit-transform: scale3d(0, 0, 0);\n          transform: scale3d(0, 0, 0);\n  -webkit-animation: moveStar 1s ease-out 0s infinite alternate;\n          animation: moveStar 1s ease-out 0s infinite alternate; }\n\n.star-animation.one {\n  bottom: 0;\n  left: -90px; }\n\n.star-animation.two {\n  top: -40px;\n  right: 70px; }\n\n.star-animation.three {\n  top: 100px;\n  right: -60px; }\n\n.star-animation.four {\n  right: 0;\n  bottom: -50px; }\n\n.star-animation.five {\n  top: 0;\n  left: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 168:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, ".popup {\n  position: absolute;\n  top: 0;\n  left: 0;\n  z-index: 10;\n  display: table;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.5); }\n\n.container-popup {\n  display: table-cell;\n  vertical-align: middle; }\n\n.content-popup {\n  position: relative;\n  width: 1004px;\n  margin: 0 auto;\n  padding: 120px 10px 70px;\n  border-radius: 4px;\n  text-align: center;\n  background-color: #ed1b3a;\n  color: #FFFFFF; }\n\n.popup-icon {\n  position: absolute;\n  z-index: 10;\n  top: -77px;\n  left: 50%;\n  margin-left: -77px;\n  border-radius: 50%;\n  box-shadow: 0px 5px 21px 0px rgba(0, 0, 0, 0.15); }\n\n.title-popup {\n  font-size: 1.7rem;\n  font-weight: bold; }\n\n.title-icon {\n  display: inline-block;\n  vertical-align: middle;\n  margin-right: 25px; }\n\n.paragraph-popup {\n  margin-top: 36px;\n  font-size: 24px;\n  line-height: 34px;\n  text-align: left; }\n\n.usecase-popup {\n  margin: 36px auto 0 auto;\n  font-size: 0;\n  line-height: 0;\n  display: table; }\n  .usecase-popup .button {\n    display: table-cell;\n    vertical-align: middle;\n    margin-bottom: 10px; }\n\n.pagination-popup {\n  width: 100%;\n  font-size: 0;\n  line-height: 0;\n  position: absolute;\n  z-index: 10;\n  bottom: -32px; }\n\n.button {\n  transition: all 0.8s ease 0s;\n  -webkit-animation: none;\n  font-weight: bold; }\n  .button.grey {\n    margin-top: 30px; }\n\n.swiper-button-prev, .swiper-button-next {\n  background-image: none; }\n\n.swiper-slide .table {\n  display: table;\n  width: 80%;\n  margin: 20px auto; }\n\n.swiper-slide p {\n  display: table-cell;\n  vertical-align: middle;\n  height: 270px;\n  font-size: 1rem;\n  line-height: 1.6; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, ".animation {\n  position: fixed;\n  right: 100px;\n  bottom: 100px;\n  z-index: 10; }\n\n.circle-left {\n  position: absolute;\n  left: 25%;\n  z-index: 10;\n  margin-left: -32px;\n  -webkit-transform: translateY(0);\n          transform: translateY(0);\n  -webkit-animation: moveSmallCircle 1.8s ease-out 0s infinite alternate-reverse;\n          animation: moveSmallCircle 1.8s ease-out 0s infinite alternate-reverse; }\n\n.circle-right {\n  position: absolute;\n  left: 75%;\n  z-index: 10;\n  margin-left: -32px;\n  -webkit-transform: translateY(0);\n          transform: translateY(0);\n  -webkit-animation: moveSmallCircle 1.8s ease-out 0s infinite alternate-reverse;\n          animation: moveSmallCircle 1.8s ease-out 0s infinite alternate-reverse; }\n\n@-webkit-keyframes moveSmallCircle {\n  0% {\n    -webkit-transform: translateY(0);\n            transform: translateY(0); }\n  100% {\n    -webkit-transform: translateY(-40px);\n            transform: translateY(-40px); } }\n\n@keyframes moveSmallCircle {\n  0% {\n    -webkit-transform: translateY(0);\n            transform: translateY(0); }\n  100% {\n    -webkit-transform: translateY(-40px);\n            transform: translateY(-40px); } }\n\n.circle-center {\n  position: absolute;\n  left: 50%;\n  z-index: 10;\n  margin-left: -50px;\n  -webkit-transform: translateY(0);\n          transform: translateY(0);\n  -webkit-animation: moveLargeCircle 1.6s ease-out 0s infinite alternate;\n          animation: moveLargeCircle 1.6s ease-out 0s infinite alternate; }\n\n@-webkit-keyframes moveLargeCircle {\n  0% {\n    -webkit-transform: translateY(0);\n            transform: translateY(0); }\n  100% {\n    -webkit-transform: translateY(-40px);\n            transform: translateY(-40px); } }\n\n@keyframes moveLargeCircle {\n  0% {\n    -webkit-transform: translateY(0);\n            transform: translateY(0); }\n  100% {\n    -webkit-transform: translateY(-40px);\n            transform: translateY(-40px); } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, ".iframe-service {\n  position: absolute;\n  top: 0;\n  left: 0;\n  z-index: 10;\n  display: block;\n  width: 100%;\n  height: 100%;\n  border: 0;\n  background: #FFFFFF; }\n\n.video-service {\n  display: block;\n  width: 100%;\n  max-width: 1200px;\n  margin: 0 auto;\n  border-radius: 2px;\n  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22); }\n\n.menu-service {\n  position: fixed;\n  left: 50%;\n  bottom: 110px;\n  z-index: 20;\n  width: 60px;\n  height: 60px;\n  margin-left: -30px;\n  border-radius: 50%;\n  background-color: #111319;\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);\n  cursor: pointer; }\n\n.img-service {\n  margin: 0 auto; }\n\n.pagination-service {\n  position: fixed;\n  left: 50%;\n  bottom: 30px;\n  z-index: 20;\n  margin-left: -290px;\n  font-size: 0;\n  line-height: 0; }\n  .pagination-service.hidden {\n    display: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, ".slider {\n  padding: 0 60px;\n  margin: 250px auto 0; }\n\n.swiper-wrapper {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding-top: 40px;\n  box-sizing: border-box; }\n\n.swiper-button-prev, .swiper-button-next {\n  width: 32px;\n  height: 56px;\n  margin-top: -28px; }\n\n.swiper-button-prev, .swiper-button-next {\n  background-image: none !important; }\n\n.swiper-button-prev {\n  left: 10%; }\n\n.swiper-button-next {\n  right: 10%; }\n\n.swiper-img {\n  position: relative;\n  top: 0;\n  left: 0;\n  z-index: 10;\n  margin: 0 auto;\n  border-radius: 50%;\n  transition: top 0.5s linear 0s; }\n\n.swiper-caption {\n  position: relative;\n  font-size: 16px;\n  line-height: 16px;\n  text-align: center;\n  opacity: 0;\n  transition: opacity 0.5s linear 0.5s;\n  width: 300px;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%); }\n  .swiper-caption::before {\n    position: absolute;\n    top: -34px;\n    left: 50%;\n    z-index: 10;\n    display: block;\n    content: \"\";\n    width: 1px;\n    height: 30px;\n    background-image: linear-gradient(#FFFFFF, #111319); }\n\n.swiper-base {\n  margin: 0 auto; }\n\n.swiper-slide.swiper-slide-active {\n  cursor: pointer; }\n  .swiper-slide.swiper-slide-active .swiper-img {\n    top: -40px;\n    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.14), 0 6px 6px rgba(0, 0, 0, 0.14); }\n  .swiper-slide.swiper-slide-active .swiper-caption {\n    opacity: 1; }\n\n.help {\n  position: fixed;\n  left: 50%;\n  bottom: 30px;\n  z-index: 10;\n  margin-left: -207px; }\n\n.img-help {\n  margin: 0 auto; }\n\n.message-help {\n  display: block;\n  margin-top: 40px;\n  font-size: 28px;\n  line-height: 28px;\n  text-transform: uppercase;\n  color: #111319;\n  -webkit-animation: bounce 0.8s linear 0s infinite alternate;\n          animation: bounce 0.8s linear 0s infinite alternate; }\n\n@-webkit-keyframes bounce {\n  0% {\n    margin-top: 20px; }\n  100% {\n    margin-top: 40px; } }\n\n@keyframes bounce {\n  0% {\n    margin-top: 20px; }\n  100% {\n    margin-top: 40px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 172:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "@-webkit-keyframes moveFlag {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0); }\n  100% {\n    -webkit-transform: translate3d(-45px, -330px, 0);\n            transform: translate3d(-45px, -330px, 0); } }\n\n@keyframes moveFlag {\n  0% {\n    -webkit-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0); }\n  100% {\n    -webkit-transform: translate3d(-45px, -330px, 0);\n            transform: translate3d(-45px, -330px, 0); } }\n\n.animation {\n  position: fixed;\n  right: 100px;\n  bottom: 100px;\n  z-index: 10; }\n\n.flag-animation {\n  position: absolute;\n  bottom: 36px;\n  left: 50%;\n  z-index: 10;\n  margin-left: -55px;\n  -webkit-animation: moveFlag 10s linear 0.5s infinite alternate;\n          animation: moveFlag 10s linear 0.5s infinite alternate; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 175:
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ 176:
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"row\">\n    <div class=\"col\">\n      \n      <header class=\"header\">\n        <a [routerLink]=\"['/slider']\" alt=\"slider page\">\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\n        </a>\n        <h1 class=\"title-header\">\n          <img src=\"assets/icon/arrow-red.png\" alt=\"\" class=\"img-title\">\n          Cadre d’architecture cible simplifié\n        </h1>\n      </header>\n      \n      <section>\n        <img src=\"assets/architecture/architecture.png\" alt=\"\" class=\"img-architecture\">\n        <button type=\"button\" class=\"button red\" [routerLink]=\"['/slider']\">Lancer le test des digital blocks</button>\n      </section>\n      \n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 177:
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"background\">\r\n  <div class=\"row\">\r\n    <div class=\"col\">\r\n      \r\n      <header class=\"header\">\r\n       <a onclick=\"window.history.back();\" alt=\"slider page\">\r\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\r\n        </a>\r\n      </header>\r\n      \r\n      <ng-container *ngFor=\"let service of services;\">\r\n        <ng-container *ngIf=\"service.id === serviceId\">\r\n          <ng-container *ngFor=\"let usecase of service.usecases;\">\r\n            <ng-container *ngIf=\"usecase.id === usecaseId\">\r\n              <section class=\"container\">\r\n                <img src=\"assets/icon/arrow-white.png\" alt=\"\" class=\"img\">\r\n                <p [textContent]=\"usecase.paragraph\" class=\"paragraph\"></p>\r\n                <button type=\"button\" class=\"button\" [routerLink]=\"['/service']\" [queryParams]=\"{url: usecase.url}\">Lancer le test !</button>\r\n                <div class=\"persona\">\r\n                  <img [src]=\"usecase.img\" alt=\"\" class=\"img-persona\">\r\n                </div>\r\n              </section>\r\n            </ng-container>\r\n          </ng-container>\r\n        </ng-container>\r\n      </ng-container>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 178:
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"background\">\r\n  <div class=\"row\">\r\n    <div class=\"col\">\r\n      \r\n      <header class=\"header\">\r\n        <a [routerLink]=\"['/slider']\" alt=\"slider page\">\r\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\r\n        </a>\r\n      </header>\r\n      \r\n      <section class=\"container\">\r\n        <img src=\"assets/icon/arrow-white.png\" alt=\"\" class=\"img\">\r\n        <h2 class=\"title\">Merci<span class=\"break\">pour votre participation !</span></h2>\r\n        <button type=\"button\" class=\"button\" [routerLink]=\"['/slider']\">Tester un autre démonstrateur</button>\r\n        <button type=\"button\" class=\"button\" [routerLink]=\"['/start']\">Terminer l’expérience</button>\r\n      </section>\r\n\r\n      <section class=\"animation\">\r\n        <img src=\"assets/end/star-1.png\" alt=\"\" class=\"star-animation one\">\r\n        <img src=\"assets/end/star-2.png\" alt=\"\" class=\"star-animation two\">\r\n        <img src=\"assets/end/star-3.png\" alt=\"\" class=\"star-animation three\">\r\n        <img src=\"assets/end/star-4.png\" alt=\"\" class=\"star-animation four\">\r\n        <img src=\"assets/end/star-5.png\" alt=\"\" class=\"star-animation five\">\r\n        <img src=\"assets/end/check.png\" alt=\"\" class=\"check-animation\">\r\n      </section>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 179:
/***/ (function(module, exports) {

module.exports = "<div class=\"popup\">\n  <div class=\"container-popup\">\n    <div class=\"content-popup\">\n\n      <section>\n\n        <ng-container *ngFor=\"let service of services;\">\n          <img src=\"assets/slider/picto-{{id}}-on.png\" alt=\"\" class=\"popup-icon\">\n          <ng-container *ngIf=\"service.id === id\">\n            <swiper *ngIf=\"slider;\"  [config]=\"{slidesPerView: 1, centeredSlides: true, prevButton: '.swiper-button-prev',    nextButton: '.swiper-button-next', noSwiping: true, autoHeight: true}\">\n              <div class=\"swiper-wrapper\">\n                <div class=\"swiper-slide\" *ngFor=\"let record of service.records;\">\n                  <h2 class=\"title-popup\">\n                    <img class=\"title-icon\" src=\"assets/icon/{{record.icon}}.png\" alt=\"\">\n                    {{ record.title }}\n                  </h2>\n                  <div class=\"table\"><p [innerHTML]=\"record.content\" class=\"paragraph-popup\"></p></div>\n                </div>\n              </div>\n              <div class=\"swiper-button-prev\"><img src=\"assets/icon/white-arrow-previous.png\" alt=\"\"></div>\n              <div class=\"swiper-button-next\"><img src=\"assets/icon/white-arrow-next.png\" alt=\"\"></div>\n            </swiper>\n\n            <div class=\"wrap-usecase-popup\" *ngIf=\"usecase;\">\n              <h2 class=\"title-popup\">\n                Choisissez le cas d’usage à tester\n              </h2>\n              <div class=\"usecase-popup\" >\n                <button type=\"button\" class=\"button red\" *ngFor=\"let usecase of service.usecases;\" [textContent]=\"usecase.title\" (click)=\"nextContext(service.id, usecase.id)\"></button>\n              </div>\n            </div>\n          </ng-container>\n        </ng-container>\n\n        <div class=\"pagination-popup\">\n          <button type=\"button\" class=\"button grey\" *ngIf=\"pagination;\" [routerLink]=\"['/slider']\">Retour</button>\n          <button type=\"button\" class=\"button red\" *ngIf=\"pagination;\" (click)=\"SetUseCase()\">Tester le digital block</button>\n          <button type=\"button\" class=\"button grey\" *ngIf=\"back;\" (click)=\"comeBack()\">Retour à la fiche</button>\n        </div>\n      </section>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 180:
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"background\">\n  <div class=\"row\">\n    <div class=\"col\">\n      \n      <header class=\"header\">\n        <a [routerLink]=\"['/slider']\" alt=\"slider page\">\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\n        </a>\n      </header>\n\n      <section class=\"container\">\n        <img src=\"assets/icon/arrow-white.png\" alt=\"\" class=\"img\">\n        <h2 class=\"title\">A travers cette expérience,<span class=\"break\">vous allez avoir l’occasion de</span>tester les digital blocks et leur<span class=\"break\">intégration dans les process !</span></h2>\n        <button type=\"button\" class=\"button\" [routerLink]=\"['/slider']\">J’ai compris, lancer l’expérience</button>\n      </section>\n\n      <div class=\"animation\">\n        <img src=\"assets/preamble/circle-small.png\" alt=\"\" class=\"circle-left\">\n        <img src=\"assets/preamble/circle-large.png\" alt=\"\" class=\"circle-center\">\n        <img src=\"assets/preamble/circle-small.png\" alt=\"\" class=\"circle-right\">\n        <img src=\"assets/preamble/base.png\" alt=\"\" class=\"base\">\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 181:
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"background\">\n  <div class=\"row\">\n    <div class=\"col\">\n\n      <header class=\"header\">\n        <a (click)=\"goBack()\" alt=\"slider page\">\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\n        </a>\n      </header>\n\n      <section>\n        <video controls=\"controls\" autoplay=\"autoplay\" *ngIf=\"enableVideo\" class=\"video-service\">\n          <source [src]=\"url\" type=\"video/mp4\">\n        </video>\n\n        <iframe [src]=\"url\" *ngIf=\"enableIframe\" class=\"iframe-service\"></iframe>\n\n        <button type=\"button\" class=\"menu-service\" (click)=\"menu()\"><img src=\"assets/icon/menu.png\" alt=\"\" class=\"img-service\"></button>\n        <div class=\"pagination-service\" [class.hidden]=\"hidden\">\n          <button type=\"button\" class=\"button grey\" (click)=\"goBack()\" >Retour à l’architecture</button>\n          <button type=\"button\" class=\"button red\" [routerLink]=\"['/end']\">Terminer le test</button>\n        </div>\n      </section>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 182:
/***/ (function(module, exports) {

module.exports = "<div>\n  <header class=\"header\">\n    <a [routerLink]=\"['/start']\" alt=\"home page\">\n      <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\n    </a>\n    <h1 class=\"title-header\">\n      <img src=\"assets/icon/arrow-red.png\" alt=\"\" class=\"img-title\">\n      Cadre d’architecture cible simplifié\n    </h1>\n  </header>\n\n  <section class=\"slider\">\n    <swiper [config]=\"config\">\n      <div class=\"swiper-wrapper\">\n\n        <div class=\"swiper-slide\">\n          <img src=\"assets/slider/temp.png\" alt=\"\" class=\"swiper-img\">\n        </div>\n\n        <div class=\"swiper-slide\">\n          <img src=\"assets/slider/temp.png\" alt=\"\" class=\"swiper-img\">\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"esignatureIsActive ? nextPopup($event, esignature) : activateEsignature()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-esignature-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">Signature électronique</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"sdcpjIsActive ? nextPopup($event, sdcpj) : activateSdcpj()\" >\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-sdcpj-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">Upload de pièces justificatives</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"radladIsActive ? nextPopup($event, radlad) : activateRadlad()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-radlad-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">RAD/ LAD (JOUVE)</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"rpaIsActive ? nextPopup($event, rpa) : activateRpa()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-rpa-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">RPA</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"gedIsActive ? nextPopup($event, ged) : activateGed()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-ged-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">GED</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"harmonieIsActive ? nextPopup($event, harmonie) : activateHarmonie()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-harmonie-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">Harmonie</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\" (click)=\"alertingIsActive ? nextPopup($event, alerting) : activateAlerting()\">\n          <figure class=\"swiper-figure\">\n            <img src=\"assets/slider/picto-alerting-on.png\" alt=\"\" class=\"swiper-img\">\n            <figcaption class=\"swiper-caption\">Alerting</figcaption>\n          </figure>\n        </div>\n\n        <div class=\"swiper-slide\">\n          <img src=\"assets/slider/temp.png\" alt=\"\" class=\"swiper-img\">\n        </div>\n\n        <div class=\"swiper-slide\">\n          <img src=\"assets/slider/temp.png\" alt=\"\" class=\"swiper-img\">\n        </div>\n\n      </div>\n      <div class=\"swiper-button-prev\"><img src=\"assets/icon/arrow-previous.png\" alt=\"\"></div>\n      <div class=\"swiper-button-next\"><img src=\"assets/icon/arrow-next.png\" alt=\"\"></div>\n    </swiper>\n    <img src=\"assets/slider/socle.png\" alt=\"\" class=\"swiper-base\">\n  </section>\n\n  <div class=\"help\">\n    <img src=\"assets/icon/arrow-grey.png\" alt=\"\" class=\"img-help\">\n    <span class=\"message-help\">Cliquez sur un digital block !</span>\n  </div>\n\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"background\">\n  <div class=\"row\">\n    <div class=\"col\">\n      \n      <header class=\"header\">\n        <a [routerLink]=\"['/slider']\" alt=\"slider page\">\n          <img src=\"assets/logo.png\" alt=\"\" class=\"img-header\">\n        </a>\n      </header>\n      \n      <section class=\"container\">\n        <img src=\"assets/icon/arrow-white.png\" alt=\"\" class=\"img\">\n        <h2 class=\"title\">Bienvenue sur le stand<span class=\"break\">de démonstration des digital</span>blocks !</h2>\n        <button type=\"button\" class=\"button\" [routerLink]=\"['/preamble']\">Lancer l’expérience</button>\n      </section>\n\n      <section class=\"animation\">\n        <img src=\"assets/start/flag.png\" alt=\"\" class=\"flag-animation\">\n        <img src=\"assets/start/base.png\" alt=\"\" class=\"base-animation\">\n      </section>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(88);


/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(61);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Config = (function () {
    function Config(http) {
        this.http = http;
    }
    Config.prototype.getData = function () {
        return this.http.get("data.json");
    };
    return Config;
}());
Config = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], Config);

var _a;
//# sourceMappingURL=config.service.js.map

/***/ }),

/***/ 60:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 60;


/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(107);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.router.navigate(['/start']);
        }, 900000);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(175),
        styles: [__webpack_require__(164)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__start_start_component__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__preamble_preamble_component__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__architecture_architecture_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__slider_slider_component__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__popup_popup_component__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__context_context_component__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_service_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__end_end_component__ = __webpack_require__(101);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var routes = [
    { path: '', redirectTo: '/start', pathMatch: 'full' },
    { path: 'start', component: __WEBPACK_IMPORTED_MODULE_9__start_start_component__["a" /* StartComponent */] },
    { path: 'preamble', component: __WEBPACK_IMPORTED_MODULE_10__preamble_preamble_component__["a" /* PreambleComponent */] },
    { path: 'architecture', component: __WEBPACK_IMPORTED_MODULE_11__architecture_architecture_component__["a" /* ArchitectureComponent */] },
    { path: 'slider', component: __WEBPACK_IMPORTED_MODULE_12__slider_slider_component__["a" /* SliderComponent */], children: [
            { path: 'popup', component: __WEBPACK_IMPORTED_MODULE_13__popup_popup_component__["a" /* PopupComponent */] }
        ] },
    { path: 'context', component: __WEBPACK_IMPORTED_MODULE_14__context_context_component__["a" /* ContextComponent */] },
    { path: 'service', component: __WEBPACK_IMPORTED_MODULE_15__service_service_component__["a" /* ServiceComponent */] },
    { path: 'end', component: __WEBPACK_IMPORTED_MODULE_16__end_end_component__["a" /* EndComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__start_start_component__["a" /* StartComponent */],
            __WEBPACK_IMPORTED_MODULE_10__preamble_preamble_component__["a" /* PreambleComponent */],
            __WEBPACK_IMPORTED_MODULE_11__architecture_architecture_component__["a" /* ArchitectureComponent */],
            __WEBPACK_IMPORTED_MODULE_12__slider_slider_component__["a" /* SliderComponent */],
            __WEBPACK_IMPORTED_MODULE_13__popup_popup_component__["a" /* PopupComponent */],
            __WEBPACK_IMPORTED_MODULE_14__context_context_component__["a" /* ContextComponent */],
            __WEBPACK_IMPORTED_MODULE_15__service_service_component__["a" /* ServiceComponent */],
            __WEBPACK_IMPORTED_MODULE_16__end_end_component__["a" /* EndComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(routes),
            __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__["SwiperModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_6__angular_common__["Location"], { provide: __WEBPACK_IMPORTED_MODULE_6__angular_common__["LocationStrategy"], useClass: __WEBPACK_IMPORTED_MODULE_6__angular_common__["HashLocationStrategy"] }, __WEBPACK_IMPORTED_MODULE_7__config_service__["a" /* Config */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArchitectureComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ArchitectureComponent = (function () {
    function ArchitectureComponent() {
    }
    ArchitectureComponent.prototype.ngOnInit = function () { };
    return ArchitectureComponent;
}());
ArchitectureComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-architecture',
        template: __webpack_require__(176),
        styles: [__webpack_require__(165)]
    }),
    __metadata("design:paramtypes", [])
], ArchitectureComponent);

//# sourceMappingURL=architecture.component.js.map

/***/ })

},[213]);
//# sourceMappingURL=main.bundle.js.map